#include <types.h>

/*-----------------------------------------------------------------------------------------------------------------------------------*/
// DECK INFORMATION
/*-----------------------------------------------------------------------------------------------------------------------------------*/

#define NUMBER_OF_CARDS										((uint8)52)							/* Texas Hold'em (No-Limit) has a deck of 52 cards */
#define NUMBER_OF_DEALT_CARDS								((uint8)2)							/* Players in Texas Hold'em (No-Limit) play with a 2-cards hand */
#define NUMBER_OF_TYPES										((uint8)4)							/* Spades - Diamonds - Hearts - Clubs */
#define NUMBER_OF_CARDS_PER_TYPE							((uint8)13)							/* A-K-Q-J-T-9-8-7-6-5-4-3-2 */

/*-----------------------------------------------------------------------------------------------------------------------------------*/
// STARTING HANDS CARDINALS NUMBERS
/*-----------------------------------------------------------------------------------------------------------------------------------*/

#define NUMBER_OF_STARTING_HANDS							((uint16)(((uint16)NUMBER_OF_CARDS * (uint16)NUMBER_OF_CARDS) / (uint16)NUMBER_OF_DEALT_CARDS))
															/* Number of starting hands:	(52*51)/2 = 1326 */
															/* We devide by 2 for the hand order purpose, AsKd is the same as KsAs */

#define NUMBER_OF_STARTING_PAIRED_HANDS						((uint8)(((uint16)NUMBER_OF_CARDS * (uint16)(NUMBER_OF_TYPES - 1)) / (uint16)NUMBER_OF_DEALT_CARDS))
															/* Number of starting pairs:	(52*3)/2 = 78 */
															/* For every chosen card from the deck (52), the second card should be the same (3) */

#define NUMBER_OF_STARTING_SUITED_HANDS						((uint16)(((uint16)NUMBER_OF_CARDS * (uint16)(NUMBER_OF_CARDS_PER_TYPE - 1)) / (uint16)NUMBER_OF_DEALT_CARDS))
															/* Number of starting suited hands: 	(52*12)/2 = 312 */
															/* For every chosen card from the deck (52), the second card must have the same type (12) */

#define NUMBER_OF_STARTING_OFF_SUITED_HANDS					((uint16)(((uint16)NUMBER_OF_CARDS * (uint16)(NUMBER_OF_CARDS_PER_TYPE - (NUMBER_OF_TYPES + NUMBER_OF_CARDS_PER_TYPE - 1))) / (uint16)NUMBER_OF_DEALT_CARDS))
															/* Number of starting off suited hands: 	(52*(52-12-4))/2 = 936 */
															/* For every chosen card from the deck (52), the second card must not have the same type and must not be the same to avoir a pair (36) */

/* WE CLEARLY VERIFY THAT NUMBER_OF_STARTING_HANDS = NUMBER_OF_STARTING_PAIRED_HANDS + NUMBER_OF_STARTING_SUITED_HANDS + NUMBER_OF_STARTING_OFF_SUITED_HANDS */
/* PAIRS, OFFSUITED and SUITED STARTING HANDS ARE THE THREE MAIN STARTING HAND IN TEXAS HOLD'EM (NO LIMIT) */

/*-----------------------------------------------------------------------------------------------------------------------------------*/
// RANGES OF STARTING HANDS CARDINALS NUMBERS
/*-----------------------------------------------------------------------------------------------------------------------------------*/

/* WHEN WE DEAL WE STARTING HANDS AND PROBABILITIES OF EACH HAND, AdKs IS THE SAME AS KhAc. THAT'S WHY WE WILL INTRODUCE THE RANGE CALCULATIONS IN THIS SECTION */
/* ANOTHER EXAMPLE TO UNDERSTAND THIS CONCEPT IS THE FOLLOWING: WHEN WE PLAY PRE FLOP, AhKh MUST BE PLAYED THE SAME WAY AS AsKs */
/* THE RANGES ARE NECESSARILY FOR A PREFLOP STRATEGY */

#define PAIR_COEFFICIENT									((uint8)6)							/* There are 6 ways to make a pair with four Aces, in pre-flop, all those pairs are considered the same */
#define SUITED_COEFFICIENT									((uint8)4)							/* There are 4 ways to make a suited hand with Aces and Kings, în pre-flop, all those suited hands are considered the same */
#define OFF_SUITED_COEFFICIENT								((uint8)12)							/* There are 12 ways to make an off suited hand with Aces and Kings, în pre-flop, all those suited hands are considered the same */

#define NUMBER_OF_RANGE_STARING_HANDS						((uint8)((NUMBER_OF_STARTING_PAIRED_HANDS / PAIR_COEFFICIENT) + (NUMBER_OF_STARTING_SUITED_HANDS / SUITED_COEFFICIENT) + (NUMBER_OF_STARTING_OFF_SUITED_HANDS / OFF_SUITED_COEFFICIENT)))
															/* The calculation of the range is deviated from the previous calculations and the different coefficients */

#define NUMBER_OF_RANGE_STARTING_PAIRED_HANDS				((uint8)(NUMBER_OF_STARTING_PAIRED_HANDS / PAIR_COEFFICIENT))
															/* The calculation of the range is deviated from the previous calculations and the different coefficients */

#define NUMBER_OF_RANGE_STARTING_SUITED_HANDS				((uint8)(NUMBER_OF_STARTING_SUITED_HANDS / SUITED_COEFFICIENT))
															/* The calculation of the range is deviated from the previous calculations and the different coefficients */

#define NUMBER_OF_RANGE_STARTING_OFF_SUITED_HANDS			((uint8)(NUMBER_OF_STARTING_OFF_SUITED_HANDS / OFF_SUITED_COEFFICIENT))
															/* The calculation of the range is deviated from the previous calculations and the different coefficients */
		